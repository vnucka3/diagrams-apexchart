import ApexChartsPresets from '../../ApexChartsPresets';

export default {
    state: {
        charts: {
            line: {},
            area: {},
            bar: {},
            horizontalBar: {},
            mix: {},
            rangeBar: {},
            candlestick: {},
            pie: {},
            donut: {},
            radar: {},
            polarArea: {},
            radialBar: {},
            scatter: {},
            heatmap: {},
            treemap: {}
        },
        chartTypes: [
            { title: 'Линии', type: 'line' },
            { title: 'Площадь / зона', type: 'area' },
            { title: 'Колонки', type: 'bar' },
            { title: 'Колонки горизонтальные', type: 'horizontalBar' },
            { title: 'Смешанные диаграммы', type: 'mix' },
            { title: 'Временная линия', type: 'rangeBar' },
            { title: 'Графики свечей', type: 'candlestick' },
            { title: 'Круговая диаграмма', type: 'pie' },
            { title: 'Круговая диаграмма пончик', type: 'donut' },
            { title: 'Диаграмма радар', type: 'radar' },
            { title: 'Диаграмма полярных областей', type: 'polarArea' },
            { title: 'Радиальная гиксограмма', type: 'radialBar' },
            { title: 'Точечные диаграммы', type: 'scatter' },
            { title: 'Тепловая диаграммы', type: 'heatmap' },
            { title: 'Древовидная диаграмма', type: 'treemap' },
        ]
    },
    getters: {
        getCharts(state) {
            return state.charts;
        },
        getChartTypes(state) {
            return state.chartTypes;
        }
    },
    mutations: {
        saveCharts(state, type) {
            if(state.charts[type].length) {
                return
            }

            if(Object.keys(state.charts[type]).length) {
                return;
            }

            state.charts[type] = ApexChartsPresets[type];
        },
    },
    actions: {
        fetchCharts({commit}, chartType) {
            commit('saveCharts', chartType);
        }
    }
}