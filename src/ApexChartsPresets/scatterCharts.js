import BasicScatterCharts from "./presets/BasicScatterCharts";

export default {
    // Точечные диаграммы
    BasicScatterCharts: {
        data: BasicScatterCharts,
        type: 'scatter',
        chartType: 'scatter'
    },
}