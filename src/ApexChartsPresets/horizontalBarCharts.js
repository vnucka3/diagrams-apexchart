// Колонки горизонтальные
import BasicBarCharts from "./presets/BasicBarCharts";
import GroupedBarCharts from "./presets/GroupedBarCharts";
import StackedBarCharts from "./presets/StackedBarCharts";
import Stacked100BarCharts from "./presets/Stacked100BarCharts";
import BarWithNegativeValuesBarCharts from "./presets/BarWithNegativeValuesBarCharts";
import ReversedBarCharts from "./presets/ReversedBarCharts";
import CustomDataLabelsBarCharts from "./presets/CustomDataLabelsBarCharts";
import PatternedBarCharts from "./presets/PatternedBarCharts";

export default {
    // Колонки горизонтальные
    BasicBarCharts: {
        data: BasicBarCharts,
        type: 'bar',
        chartType: 'horizontalBar'
    },
    GroupedBarCharts: {
        data: GroupedBarCharts,
        type: 'bar',
        chartType: 'horizontalBar'
    },
    StackedBarCharts: {
        data: StackedBarCharts,
        type: 'bar',
        chartType: 'horizontalBar'
    },
    Stacked100BarCharts: {
        data: Stacked100BarCharts,
        type: 'bar',
        chartType: 'horizontalBar'
    },
    BarWithNegativeValuesBarCharts: {
        data: BarWithNegativeValuesBarCharts,
        type: 'bar',
        chartType: 'horizontalBar'
    },
    ReversedBarCharts: {
        data: ReversedBarCharts,
        type: 'bar',
        chartType: 'horizontalBar'
    },
    CustomDataLabelsBarCharts: {
        data: CustomDataLabelsBarCharts,
        type: 'bar',
        chartType: 'horizontalBar'
    },
    PatternedBarCharts: {
        data: PatternedBarCharts,
        type: 'bar',
        chartType: 'horizontalBar'
    }
}