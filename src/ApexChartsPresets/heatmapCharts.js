import BasicHeatmapCharts from "./presets/BasicHeatmapCharts";
import MultipleSeriesHeatmapCharts from "./presets/MultipleSeriesHeatmapCharts";
import ColorRangeHeatmapCharts from "./presets/ColorRangeHeatmapCharts";
import RoundedHeatmapCharts from "./presets/RoundedHeatmapCharts";

export default {
    // Тепловая диаграммы
    BasicHeatmapCharts: {
        data: BasicHeatmapCharts,
        type: 'heatmap',
        chartType: 'heatmap'
    },
    MultipleSeriesHeatmapCharts: {
        data: MultipleSeriesHeatmapCharts,
        type: 'heatmap',
        chartType: 'heatmap'
    },
    ColorRangeHeatmapCharts: {
        data: ColorRangeHeatmapCharts,
        type: 'heatmap',
        chartType: 'heatmap'
    },
    RoundedHeatmapCharts: {
        data: RoundedHeatmapCharts,
        type: 'heatmap',
        chartType: 'heatmap'
    },
}