// Площадь / зона
import SplineAreaCharts from "./presets/SplineAreaCharts";
import DatetimeXAxisAreaChart from "./presets/DatetimeXAxisAreaChart";
import NegativeAreaCharts from "./presets/NegativeAreaCharts";
import MissingNullValuesAreaCharts from "./presets/MissingNullValuesAreaCharts";

export default {
    // Площадь / зона
    SplineAreaCharts: {
        data: SplineAreaCharts,
        type: 'area',
        chartType: 'area'
    },
    DatetimeXAxisAreaChart: {
        data: DatetimeXAxisAreaChart,
        type: 'area',
        chartType: 'area'
    },
    NegativeAreaCharts: {
        data: NegativeAreaCharts,
        type: 'area',
        chartType: 'area'
    },
    MissingNullValuesAreaCharts: {
        data: MissingNullValuesAreaCharts,
        type: 'area',
        chartType: 'area'
    },
}
