import BasicRadarCharts from "./presets/BasicRadarCharts";
import MultipleSeriesRadarCharts from "./presets/MultipleSeriesRadarCharts";
import PolygonFillRadarCharts from "./presets/PolygonFillRadarCharts";

export default {
    // Радар диаграмма
    BasicRadarCharts: {
        data: BasicRadarCharts,
        type: 'radar',
        chartType: 'radar'
    },
    MultipleSeriesRadarCharts: {
        data: MultipleSeriesRadarCharts,
        type: 'radar',
        chartType: 'radar'
    },
    PolygonFillRadarCharts: {
        data: PolygonFillRadarCharts,
        type: 'radar',
        chartType: 'radar'
    },
}