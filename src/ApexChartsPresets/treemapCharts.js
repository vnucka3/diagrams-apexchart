import BasicTreemapCharts from "./presets/BasicTreemapCharts";
import MultipleSeriesTreemapCharts from "./presets/MultipleSeriesTreemapCharts";
import ColorRangeTreemapCharts from "./presets/ColorRangeTreemapCharts";
import DistributedTreemapCharts from "./presets/DistributedTreemapCharts";

export default {
    // Древовидная диаграмма
    BasicTreemapCharts: {
        data: BasicTreemapCharts,
        type: 'treemap',
        chartType: 'treemap'
    },
    MultipleSeriesTreemapCharts: {
        data: MultipleSeriesTreemapCharts,
        type: 'treemap',
        chartType: 'treemap'
    },
    ColorRangeTreemapCharts: {
        data: ColorRangeTreemapCharts,
        type: 'treemap',
        chartType: 'treemap'
    },
    DistributedTreemapCharts: {
        data: DistributedTreemapCharts,
        type: 'treemap',
        chartType: 'treemap'
    },
}