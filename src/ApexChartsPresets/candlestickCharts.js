import BasicCandlestickCharts from "./presets/BasicCandlestickCharts";

export default {
    // Графики свечей
    BasicCandlestickCharts: {
        data: BasicCandlestickCharts,
        type: 'candlestick',
        chartType: 'candlestick'
    },
}