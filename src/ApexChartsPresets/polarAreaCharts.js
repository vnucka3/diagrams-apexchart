import BasicPolarAreaCharts from "./presets/BasicPolarAreaCharts";
import MonochromePolarAreaCharts from "./presets/MonochromePolarAreaCharts";

export default {
    // Диаграмма полярных областей
    BasicPolarAreaCharts: {
        data: BasicPolarAreaCharts,
        type: 'polarArea',
        chartType: 'polarArea'
    },
    MonochromePolarAreaCharts: {
        data: MonochromePolarAreaCharts,
        type: 'polarArea',
        chartType: 'polarArea'
    },
}