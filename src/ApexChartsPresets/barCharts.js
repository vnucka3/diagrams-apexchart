// Колонки
import BasicColumnCharts from "./presets/BasicColumnCharts";
import ColumnWithDataLabelsColumnCharts from "./presets/ColumnWithDataLabelsColumnCharts";
import StackedColumnCharts from "./presets/StackedColumnCharts";
import Stacked100ColumnCharts from "./presets/Stacked100ColumnCharts";
import ColumnWithRotatedLabelsColumnCharts from "./presets/ColumnWithRotatedLabelsColumnCharts";
import DistributedColumnCharts from "./presets/DistributedColumnCharts";

export default {
    // Колонки
    BasicColumnCharts: {
        data: BasicColumnCharts,
        type: 'bar',
        chartType: 'bar'
    },
    ColumnWithDataLabelsColumnCharts: {
        data: ColumnWithDataLabelsColumnCharts,
        type: 'bar',
        chartType: 'bar'
    },
    StackedColumnCharts: {
        data: StackedColumnCharts,
        type: 'bar',
        chartType: 'bar'
    },
    Stacked100ColumnCharts: {
        data: Stacked100ColumnCharts,
        type: 'bar',
        chartType: 'bar'
    },
    ColumnWithRotatedLabelsColumnCharts: {
        data: ColumnWithRotatedLabelsColumnCharts,
        type: 'bar',
        chartType: 'bar'
    },
    DistributedColumnCharts: {
        data: DistributedColumnCharts,
        type: 'bar',
        btnType: 'bar'
    }
}