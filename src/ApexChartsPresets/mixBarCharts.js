// Микс линии + колонки
import LineColumnMixedCharts from "./presets/LineColumnMixedCharts";
import LineAreaMixedCharts from "./presets/LineAreaMixedCharts";
import LineColumnAreaMixedCharts from "./presets/LineColumnAreaMixedCharts";
import LineScatterMixedCharts from "./presets/LineScatterMixedCharts";

export default {
    // Смешанные диаграммы
    LineColumnMixedCharts: {
        data: LineColumnMixedCharts,
        type: 'line',
        chartType: 'mix'
    },
    LineAreaMixedCharts: {
        data: LineAreaMixedCharts,
        type: 'line',
        chartType: 'mix'
    },
    LineColumnAreaMixedCharts: {
        data: LineColumnAreaMixedCharts,
        type: 'line',
        chartType: 'mix'
    },
    LineScatterMixedCharts: {
        data: LineScatterMixedCharts,
        type: 'line',
        chartType: 'mix'
    },
}