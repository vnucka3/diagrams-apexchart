// Линии
import BasicLineCharts from "./presets/BasicLineCharts";
import BasicMultiLineCharts from "./presets/BasicMultiLineCharts";
import LineWithDataLabelsLineCharts from "./presets/LineWithDataLabelsLineCharts";
import SteplineLineCharts from "./presets/SteplineLineCharts";
import GradientLineCharts from "./presets/GradientLineCharts";
import LineDashedLineCharts from "./presets/LineDashedLineCharts";
import LineMissingNullValuesLineCharts from "./presets/LineMissingNullValuesLineCharts";

export default {
    // Линии
    BasicLineCharts: {
        data: BasicLineCharts,
        type: 'line',
        chartType: 'line'
    },
    BasicMultiLineCharts: {
        data: BasicMultiLineCharts,
        type: 'line',
        chartType: 'line'
    },
    LineWithDataLabelsLineCharts: {
        data: LineWithDataLabelsLineCharts,
        type: 'line',
        chartType: 'line'
    },
    SteplineLineCharts: {
        data: SteplineLineCharts,
        type: 'line',
        chartType: 'line'
    },
    GradientLineCharts: {
        data: GradientLineCharts,
        type: 'line',
        chartType: 'line'
    },
    LineDashedLineCharts: {
        data: LineDashedLineCharts,
        type: 'line',
        chartType: 'line'
    },
    LineMissingNullValuesLineCharts: {
        data: LineMissingNullValuesLineCharts,
        type: 'line',
        chartType: 'line'
    }
}