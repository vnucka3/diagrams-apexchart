const generateData = function (items) {
    const arr = [];
    for(let i = 0; i < items; i++) {
        arr.push(Math.floor(Math.random() * 100));
    }

    return arr;
}

const generateColors = function (items) {
    const arr = [];
    for(let i = 0; i < items; i++) {
        arr.push('#'+Math.random().toString(16).substr(2,6));
    }

    return arr;
}


export default {
    series: [{
        name: 'Metric1',
        data: generateData(8, {
            min: 0,
            max: 90
        })
        },
        {
            name: 'Metric2',
            data: generateData(8, {
                min: 0,
                max: 90
            })
        },
        {
            name: 'Metric3',
            data: generateData(8, {
                min: 0,
                max: 90
            })
        },
        {
            name: 'Metric4',
            data: generateData(8, {
                min: 0,
                max: 90
            })
        },
        {
            name: 'Metric5',
            data: generateData(8, {
                min: 0,
                max: 90
            })
        },
        {
            name: 'Metric6',
            data: generateData(8, {
                min: 0,
                max: 90
            })
        },
        {
            name: 'Metric7',
            data: generateData(8, {
                min: 0,
                max: 90
            })
        },
        {
            name: 'Metric8',
            data: generateData(8, {
                min: 0,
                max: 90
            })
        },
    ],
    chartOptions: {
        chart: {
            height: 450,
            type: 'heatmap',
        },
        dataLabels: {
            enabled: false
        },
        colors: generateColors(20),
        xaxis: {
            type: 'category',
            categories: ['10:00', '10:30', '11:00', '11:30', '12:00', '12:30', '01:00', '01:30']
        },
        title: {
            text: 'HeatMap Chart (Different color shades for each series)'
        },
        grid: {
            padding: {
                right: 20
            }
        }
    },
}