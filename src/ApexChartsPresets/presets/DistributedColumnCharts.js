export default {
    series: [{
        data: [21, 22, 10, 28, 16, 21, 13, 30]
    }],
    chartOptions: {
        chart: {
            height: 350,
            type: 'bar',
            events: {
                click: function(chart, w, e) {
                    // console.log(chart, w, e)
                }
            }
        },
        colors: ['#269ffb', '#26e7a5', '#febb3b', '#ff6077', '#8b75d7', '#6d838d', '#46b3a9', '#d730eb'],
        plotOptions: {
            bar: {
                columnWidth: '45%',
                distributed: true
            }
        },
        dataLabels: {
            enabled: false
        },
        legend: {
            show: false
        },
        xaxis: {
            categories: [
                ['John', 'Doe'],
                ['Joe', 'Smith'],
                ['Jake', 'Williams'],
                'Amber',
                ['Peter', 'Brown'],
                ['Mary', 'Evans'],
                ['David', 'Wilson'],
                ['Lily', 'Roberts'],
            ],
            labels: {
                style: {
                    colors: ['#269ffb', '#26e7a5', '#febb3b', '#ff6077', '#8b75d7', '#6d838d', '#46b3a9', '#d730eb'],
                    fontSize: '12px'
                }
            }
        }
    }
}