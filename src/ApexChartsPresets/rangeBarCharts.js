import BasicTimelineCharts from "./presets/BasicTimelineCharts";
import DistributedTimelineCharts from "./presets/DistributedTimelineCharts";
import MultiSeriesTimelineCharts from "./presets/MultiSeriesTimelineCharts";
import AdvancedTimelineCharts from "./presets/AdvancedTimelineCharts";
import MultipleSeriesGroupRowsTimelineCharts from "./presets/MultipleSeriesGroupRowsTimelineCharts";

export default {
    // Временная линия
    BasicTimelineCharts: {
        data: BasicTimelineCharts,
        type: 'rangeBar',
        chartType: 'rangeBar'
    },
    DistributedTimelineCharts: {
        data: DistributedTimelineCharts,
        type: 'rangeBar',
        chartType: 'rangeBar'
    },
    MultiSeriesTimelineCharts: {
        data: MultiSeriesTimelineCharts,
        type: 'rangeBar',
        chartType: 'rangeBar'
    },
    AdvancedTimelineCharts: {
        data: AdvancedTimelineCharts,
        type: 'rangeBar',
        chartType: 'rangeBar'
    },
    MultipleSeriesGroupRowsTimelineCharts: {
        data: MultipleSeriesGroupRowsTimelineCharts,
        type: 'rangeBar',
        chartType: 'rangeBar'
    },
}