import BasicRadialBarCharts from "./presets/BasicRadialBarCharts";
import MultipleRadialBarCharts from "./presets/MultipleRadialBarCharts";
import CustomAngleCircleRadialBarCharts from "./presets/CustomAngleCircleRadialBarCharts";
import GradientRadialBarCharts from "./presets/GradientRadialBarCharts";
import StrokedGaugeRadialBarCharts from "./presets/StrokedGaugeRadialBarCharts";
import SemiCircleGaugeRadialBarCharts from "./presets/SemiCircleGaugeRadialBarCharts";

export default {
    // Радиальная гиксограмма
    BasicRadialBarCharts: {
        data: BasicRadialBarCharts,
        type: 'radialBar',
        chartType: 'radialBar'
    },
    MultipleRadialBarCharts: {
        data: MultipleRadialBarCharts,
        type: 'radialBar',
        chartType: 'radialBar'
    },
    CustomAngleCircleRadialBarCharts: {
        data: CustomAngleCircleRadialBarCharts,
        type: 'radialBar',
        chartType: 'radialBar'
    },
    GradientRadialBarCharts: {
        data: GradientRadialBarCharts,
        type: 'radialBar',
        chartType: 'radialBar'
    },
    StrokedGaugeRadialBarCharts: {
        data: StrokedGaugeRadialBarCharts,
        type: 'radialBar',
        chartType: 'radialBar'
    },
    SemiCircleGaugeRadialBarCharts: {
        data: SemiCircleGaugeRadialBarCharts,
        type: 'radialBar',
        chartType: 'radialBar'
    },
}