import SimplePieChart from "./presets/SimplePieChart";
import MonochromePieChart from "./presets/MonochromePieChart";

export default {
    SimplePieChart: {
        data: SimplePieChart,
        type: 'pie',
        chartType: 'pie'
    },
    MonochromePieChart: {
        data: MonochromePieChart,
        type: 'pie',
        chartType: 'pie'
    },
}