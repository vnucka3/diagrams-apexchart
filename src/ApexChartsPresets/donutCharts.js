import SimpleDonutPieChart from "./presets/SimpleDonutPieChart";
import GradientDonutPieChart from "./presets/GradientDonutPieChart";
import DonutWithPatternPieChart from "./presets/DonutWithPatternPieChart";

export default {
    SimpleDonutPieChart: {
        data: SimpleDonutPieChart,
        type: 'donut',
        chartType: 'pie'
    },
    GradientDonutPieChart: {
        data: GradientDonutPieChart,
        type: 'donut',
        chartType: 'pie'
    },
    DonutWithPatternPieChart: {
        data: DonutWithPatternPieChart,
        type: 'donut',
        chartType: 'pie'
    },
}