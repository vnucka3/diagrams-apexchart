import areaCharts from "./areaCharts";
import barCharts from "./barCharts";
import candlestickCharts from "./candlestickCharts";
import heatmapCharts from "./heatmapCharts";
import horizontalBarCharts from "./horizontalBarCharts";
import lineCharts from "./lineCharts";
import mixBarCharts from "./mixBarCharts";
import pieCharts from "./pieCharts";
import donutCharts from "./donutCharts";
import polarAreaCharts from "./polarAreaCharts";
import radialBarCharts from "./radialBarCharts";
import radarCharts from "./radarCharts";
import scatterCharts from "./scatterCharts";
import rangeBarCharts from "./rangeBarCharts";
import treemapCharts from "./treemapCharts";

export default {
    line: lineCharts,
    area: areaCharts,
    bar: barCharts,
    horizontalBar: horizontalBarCharts,
    mix: mixBarCharts,
    rangeBar: rangeBarCharts,
    candlestick: candlestickCharts,
    pie: pieCharts,
    donut: donutCharts,
    radar: radarCharts,
    polarArea: polarAreaCharts,
    radialBar: radialBarCharts,
    scatter: scatterCharts,
    heatmap: heatmapCharts,
    treemap: treemapCharts
}